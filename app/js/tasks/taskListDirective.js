(function() {
    var taskListController = function($scope, $timeout) {
        $scope.selectedIndex = -1;
        $scope.editingIndex = -1;
        $scope.startDate = $scope.startInit();
        $scope.endDate = $scope.endInit();

        var middleTime = $scope.endDate.getTime() - $scope.startDate.getTime();
        $scope.targetDate = new Date($scope.startDate.getTime() + middleTime / 2);

        $scope.filterTasks = function(task) {
            var target = task.targetDate.getTime();
            return target >= $scope.startDate.getTime() &&
                target < $scope.endDate.getTime();
        };

        $scope.addTask = function() {
            $scope.tasks.push({
                id : app.utils.uuid(),
                title : "New todo",
                active: true,
                done: false,
                category: "foo",
                targetDate: new Date($scope.targetDate.getTime())
            });
        };

        $scope.selectTask = function(index, task) {
            if ($scope.selectedIndex == index)
                $scope.selectedIndex = -1;
            else
                $scope.selectedIndex = index;

        };

        $scope.editTask = function(index, task) {
            if (task.done) return;
            $scope.editingIndex = index;
        };

        $scope.activateTask = function(index, task) {
            task.active = !task.active;
        };

        $scope.isSelected = function(index, task) {
            if ($scope.selectedIndex != index) return false;
            if (!task.description || task.description.length < 5) return false;
            return true;
        }

        $scope.draggableData = function(task) {
            return {
                "task" : task,
                "dragData" : $scope.dragData
            };
        };
    };

    angular.module("taskLists.directive", [])
        .directive("taskList", function($timeout) {
            return {
                restrict: "E",
                scope: {
                    "tasks": "=",
                    "dragData" : "=",
                    "startInit": "&",
                    "endInit": "&",
                    "title": "@"
                },
                templateUrl: "partials/taskList.html",
                link: function(scope, element, attrs) {

                    scope.$watch('editingIndex', function(newValue) {
                        var i = newValue;
                        var input = $(element.find("input")[i]);
                        setTimeout(function() {
                            input.focus();
                            input.select();
                        }, 50);
                    })

                    element.on('keypress', 'input', function(evt) {
                        if (evt.which == 13) {
                            scope.$apply(function() {
                                scope.editingIndex = -1;
                            });
                        }
                    });

                    element.find(".tasks-box").droppable({
                        accept: ".task",
                        activeClass: "active",
                        hoverClass: "selected",
                        drop: function(event, ui) {
                            scope.$apply(function() {
                                scope.dragData.current.targetDate = new Date(scope.targetDate.getTime());
                                console.log(scope.dragData.current);
                            });
                        }
                    });
                },
                controller: taskListController
            };
        })
        .directive('flyoutHover', function() {
            return {
                link: function(scope, element, attrs) {
                    var flyout = element.find(attrs.flyoutHover);
                    var timeout = undefined;
                    element.mouseover(function() {
                        $(attrs.flyoutHover).hide();
                        flyout.show();
                    });
                    var clearFunc = function() {
                        if (timeout) clearTimeout(timeout);
                    }
                    var leaveFunc = function() {
                        clearFunc();
                        timeout = setTimeout(function() {
                            flyout.hide();
                        }, 200);
                    };
                    element.mouseleave(leaveFunc);
                    flyout.mouseover(clearFunc);
                    flyout.mouseleave(leaveFunc);
                }
            }
        });
})();
