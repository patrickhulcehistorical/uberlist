angular.module("tasks.controller", [])
    .controller("TasksController", ["$scope", function ($scope) {
        $scope.tasks = [{
            id : "abc",
            title : "Do the dishes",
            description: "Wash everything...",
            active: true,
            done: false,
            category: "foo",
            targetDate: new Date(2013,11,20)
        },{
            id : "abce",
            title : "Do the dishes (Inactive)",
            description: "Wash everything...",
            active: false,
            done: false,
            category: "foo",
            targetDate: new Date(2013,11,20)
        },{
            id : "abcd",
            title : "Another task",
            description: "Wash everything...",
            active: true,
            done: false,
            category: "foo",
            targetDate: new Date(2013,11,20)
        },{
            id : "abcf",
            title : "Finished task",
            description: "Wash everything...",
            active: true,
            done: true,
            category: "foo",
            targetDate: new Date(2013,11,22)
        }];

        $scope.dragData = {
            current: -1
        };

        $scope.startDate = new Date(2013,11,19,3);
        $scope.beginningDate = new Date(2010,1,1);

        $scope.addDays = function(date,offsetInDays) {
            var offsetInMiliseconds = offsetInDays * 24 * 60 * 60 * 1000;
            return new Date(date.getTime() + offsetInMiliseconds);
        };
    }]);