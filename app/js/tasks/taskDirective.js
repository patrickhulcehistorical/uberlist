angular.module("tasks.directive", [])
    .directive('taskDraggable', function () {
        return {
            link: function (scope, element, attrs) {
                element.draggable({
                    revert: true,
                    revertDuration: 0,
                    zIndex: 15,
                    start: function () {
                        scope.dragData.current = scope.task;
                    }
                });
            }
        };
    });