(function(app) {
    app.angular = angular.module("uberlist",['tasks.controller','taskLists.directive','tasks.directive']);

    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    };

    function uuid() {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };

    app.utils = {
        "uuid" : uuid
    };
})(window.app = window.app || {});
